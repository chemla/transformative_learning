import argparse, pdb, os
import pytorch_lightning as pl
from pytorch_lightning.callbacks import LearningRateMonitor, ModelCheckpoint, ModelSummary
from transformative_learning.data.img import MNISTDataModule
from transformative_learning import models
from transformative_learning.monitor import ReconstructionMonitor, DissectionMonitor
import tensorflow as tf
import tensorboard as tb
from transformative_learning.utils.misc import checkdir
import gin.torch


# Arguments
parser = argparse.ArgumentParser()
parser.add_argument("config", type=str, help="GIN configuration file")
parser.add_argument('name', type=str, default="transformative_mnist")
parser.add_argument("--output", type=str, default = "runs/")
parser.add_argument("--checkpoint", type=str, default=None)
parser.add_argument('--save_n_epochs', type=int, default = None)

args = parser.parse_args()
gin.bind_parameter('%INPUT_SHAPE', [1, 28, 28])
gin.parse_config_file(args.config)

# Set up model
data = MNISTDataModule()#num_workers=args.num_workers)
# if args.checkpoint is None:
#     model = ModulationMorpher(**config.model.dict())
#     save_config(config, save_path, config.save.name)
# else:
#     # config = Config(f"{os.path.dirname(args.checkpoint)}/{os.path.splitext(os.path.basename(args.checkpoint))[0]}.yaml")
# model = ModulationMorpher.load_from_checkpoint(args.checkpoint)

model = models.ModulationMorpher()

# Configure callbacks
lr_monitor = LearningRateMonitor(logging_interval='epoch')
save_monitor = ModelCheckpoint(f"{args.output}/{args.name}", args.name, monitor="loss/valid", every_n_epochs=args.save_n_epochs)
callbacks = [lr_monitor, save_monitor, ReconstructionMonitor(), DissectionMonitor()]

# # Train!
trainer = pl.Trainer(callbacks=callbacks)
# if args.check:
#     pdb.set_trace()
trainer.fit(model, datamodule=data)

