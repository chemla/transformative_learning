import random, copy, torch, os, sys, bisect, functools, torch.distributions as dist



def get_random_hash():
    """Generate random hash"""
    return ''.join([chr(random.randrange(97, 122)) for _ in range(10)])


def checklist(item, n=1, copy=False):
    """Repeat list elemnts
    """
    if not isinstance(item, list):
        if copy:
            item = [copy.deepcopy(item) for _ in range(n)]
        else:
            item = [item]*n
    return item

def checktuple(item, n=1, copy=False):
    """Check tuple"""
    if not isinstance(item, tuple):
        if copy:
            item = tuple([copy.deepcopy(item) for _ in range(n)])
        else:
            item = tuple([item]*n)
    return item

def checkdir(directory):
    """Check directory existence. Create a directory if necessary
    """
    directory = os.path.abspath(directory)
    if not os.path.isdir(directory):
        os.makedirs(directory)


def kronecker(A, B):
    """
    Kronecker product of two incoming matrices A and B
    Args:
        A (torch.Tensor): b x _ x _ matrix
        B (torch.Tensor): b x _ x _ matrix

    Returns:
        out (torch.Tensor): Kronkecker product
    """
    assert len(A.shape) == len(B.shape) == 3, "kroncker takes b x _ x _ matrices"
    requires_grad = A.requires_grad or B.requires_grad
    out = torch.zeros(A.shape[0], A.size(1)*B.size(1),  A.size(2)*B.size(2), requires_grad=requires_grad, device=A.device)
    for i in range(A.shape[0]):
        out[i] =  torch.einsum("ab,cd->acbd", A[i], B[i]).contiguous().view(A.size(1)*B.size(1),  A.size(2)*B.size(2))
    return out

def make_video(figures: list, fps=2, out=None):
    if os.system('ffmpeg -h > /tmp/dumb') != 0:
        raise Exception('ffmpeg does not seem to be installed on this computer (required for make_piano_roll_anim)')
    hash = get_random_hash(); out_dir = "/tmp/"+hash
    checkdir(out_dir); paths = []
    for i, fig in enumerate(figures):
        paths.append(f"{out_dir}/{i:09}.jpg")
        fig.savefig(paths[-1], format="jpg")
    if out is None:
        out = out_dir+"/video.mp4"
    os.system(f'ffmpeg -r {fps} -i {out_dir}/%09d.jpg -vcodec mpeg4 -y {out}')
    return out

def print_stats(name, tensor):
    print(name, tensor.min().item(), tensor.max().item(), tensor.mean().item(), tensor.std().item())

def print_module_stats(module):
    for k, v in module.named_parameters():
        print_stats(k, v)

def print_module_grads(module):
    for k, v in module.named_parameters():
        if v.grad is None:
            print(k, 'has no grad')
        else:
            print_stats(k, v.grad)


def checkdist(obj):
    if isinstance(obj, str):
        return getattr(dist, obj)
    elif issubclass(obj, dist.Distribution):
        return obj
    else:
        raise TypeError('obj %s does not seem to be a distribution')
