import torch, torch.distributions as dist, torchvision as tv, pdb
import pytorch_lightning as pl
from pytorch_lightning.callbacks import Callback
from pytorch_lightning.trainer.states import RunningStage, TrainerFn, TrainerState, TrainerStatus

class ReconstructionMonitor(Callback):

    def __init__(self, n_reconstructions=5, n_samples=10, monitor_epochs=1):
        self.n_reconstructions = n_reconstructions
        self.n_samples = n_samples
        self.monitor_epochs = monitor_epochs

    def plot_morphings(self, model, x_1, x_2):
        out, _ = model.morph(x_1, x_2)
        if isinstance(out, dist.Distribution):
            out = out.mean
        full_img = torch.cat([x_1, x_2, out], dim=0)
        full_img = tv.utils.make_grid(full_img, nrow=x_1.size(0))
        return full_img

    def plot_samples(self, model, x_1,  n_samples=10):
        out, z = model.sample_morphs(x_1, n_samples=n_samples)
        if isinstance(out, dist.Distribution):
            out = out.mean
        x_1 = x_1.unsqueeze(1)
        out = torch.cat([x_1, out], dim=1)
        out = out.reshape(out.size(0) * out.size(1), *out.shape[2:])
        full_img = tv.utils.make_grid(out, nrow=n_samples + 1)
        return full_img

    def on_validation_epoch_end(self, trainer: pl.Trainer, pl_module: pl.LightningModule):
        print("--launching generations...")
        with torch.no_grad():
            if trainer.state.stage == RunningStage.SANITY_CHECKING:
                return
            if trainer.current_epoch % self.monitor_epochs != 0:
                return
            # plot training example
            (x_1, x_2), (y_1, y_2) = next(trainer.datamodule.train_dataloader(batch_size=self.n_reconstructions).__iter__())
            x_1 = x_1.to(pl_module.device); x_2 = x_2.to(pl_module.device)
            trainer.logger.experiment.add_image("morphings_train", self.plot_morphings(trainer.model, x_1, x_2), trainer.current_epoch)
            trainer.logger.experiment.add_image("sample_train", self.plot_samples(trainer.model, x_1, n_samples=self.n_samples), trainer.current_epoch)
            # plot validation examples
            (x_1, x_2), (y_1, y_2) = next(trainer.datamodule.val_dataloader(batch_size=self.n_reconstructions).__iter__())
            x_1 = x_1.to(pl_module.device); x_2 = x_2.to(pl_module.device)
            trainer.logger.experiment.add_image("morphings_val", self.plot_morphings(trainer.model, x_1, x_2), trainer.current_epoch)
            trainer.logger.experiment.add_image("sample_val", self.plot_samples(trainer.model, x_1, n_samples=self.n_samples), trainer.current_epoch)
            print("--generation ended")




