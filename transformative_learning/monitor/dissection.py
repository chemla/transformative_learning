import torch, torch.distributions as dist, torchvision as tv, sys, pdb
sys.path.append("../")
import pytorch_lightning as pl
from pytorch_lightning.callbacks import Callback
from pytorch_lightning.trainer.states import RunningStage, TrainerFn, TrainerState, TrainerStatus


def update_dict(memory, new_dict):
    for k, v in new_dict.items():
        if k in memory.keys():
            memory[k].append(v)
        else:
            memory[k] = [v]
    return memory

class DissectionMonitor(Callback):

    def __init__(self, n_samples=1000, monitor_epochs=1):
        self.n_samples = n_samples
        self.batch_size = 1024
        self.monitor_epochs = monitor_epochs

    def on_validation_epoch_end(self, trainer: pl.Trainer, pl_module: pl.LightningModule):
        with torch.no_grad():
            if trainer.state.stage == RunningStage.SANITY_CHECKING:
                return
            if trainer.current_epoch % self.monitor_epochs != 0:
                return

            # plot model parameters distribution
            trace = {}
            for k, v  in trainer.model.named_parameters():
                trainer.logger.experiment.add_histogram("params/"+k, v, global_step=trainer.current_epoch)
            n_batch = 0
            datas = []
            latents = []; latents_std = []
            metadatas = []
            current_trace = {}
            for (x_1, x_2), (y_1, y_2) in trainer.datamodule.train_dataloader(batch_size=self.batch_size):
                x_1 = x_1.to(pl_module.device); x_2 = x_2.to(pl_module.device)
                out, z = trainer.model.morph(x_1, x_2, trace=current_trace)
                datas.append(torch.cat([x_1, x_2], -1))
                metadatas.append(y_1)
                if isinstance(z, dist.Normal):
                    latents.append(z.mean)
                    latents_std.append(z.stddev)
                else:
                    raise NotImplementedError
                n_batch += x_1.size(0)
                trace = update_dict(trace, current_trace)
                if n_batch > self.n_samples:
                    break

            # add outputs
            for k, v in trace.items():
                trainer.logger.experiment.add_histogram(f"outs/{k}", torch.cat(v, 0), global_step = trainer.current_epoch)
            # add latents
            latents = torch.cat(latents, 0)
            latents_std = torch.cat(latents_std, 0)
            
            for dim in range(latents.size(-1)):
                trainer.logger.experiment.add_histogram("latent/mean_"+str(dim), latents[..., dim], global_step=trainer.current_epoch)
                trainer.logger.experiment.add_histogram("latent/std_"+str(dim), latents_std[..., dim], global_step=trainer.current_epoch)
            datas = torch.cat(datas, 0)
            metadatas = torch.cat(metadatas, 0)
            trainer.logger.experiment.add_embedding(latents, tag="latent", label_img=datas, global_step=trainer.current_epoch)
            print("--dissection ended")





