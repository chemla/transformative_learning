from torch.utils.data import DataLoader, random_split

class TransformativeDataLoader(DataLoader):
    def __init__(self, dataset, *args, pairing=None, **kwargs):
        super(TransformativeDataLoader, self).__init__(dataset, *args, **kwargs)
        self.pairing = pairing or "many-to-many"
        self.loader1 = DataLoader(dataset, *args, shuffle=True, **kwargs)
        kwargs = dict(kwargs)
        if self.pairing == "single-to-many":
            kwargs["batch_size"] = 1
        self.loader2 = DataLoader(dataset, *args, shuffle=True, **kwargs)

    def __iter__(self):
        iter2 = self.loader2.__iter__()
        for x_1, y_1 in self.loader1:
            x_2, y_2 = next(iter2)
            yield (x_1, x_2), (y_1, y_2)


