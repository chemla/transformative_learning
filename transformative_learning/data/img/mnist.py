import os, sys
sys.path.append('..')
from transformative_learning.data import TransformativeDataLoader
from torch.utils.data import random_split
from torchvision.datasets import MNIST
from torchvision import transforms
from pytorch_lightning import LightningDataModule


class MNISTDataModule(LightningDataModule):
    def __init__(self, 
                 batch_size = 64,
                 pairing = "many-to-many",
                 bounds = [0, 1],
                 binary = False,
                 **kwargs):
        super().__init__()
        self.batch_size = batch_size or 64
        self.pairing = pairing
        self.range = bounds or [0, 1]
        self.binary = binary 
        self.loader_args = kwargs

    # When doing distributed training, Datamodules have two optional arguments for
    # granular control over download/prepare/splitting data:

    # OPTIONAL, called only on 1 GPU/machine
    def prepare_data(self):
        MNIST(os.getcwd(), train=True, download=True)
        MNIST(os.getcwd(), train=False, download=True)

    # OPTIONAL, called for every GPU/machine (assigning state is OK)
    def setup(self, stage = None):
        # transforms
        if self.binary:
            transform = transforms.Compose([transforms.ToTensor(), transforms.Lambda(lambda x: (x > 0.5).float())])
        else:
            transform = transforms.Compose([transforms.ToTensor()])
        # split dataset
        if stage in (None, "fit"):
            mnist_train = MNIST(os.getcwd(), train=True, transform=transform)
            self.mnist_train, self.mnist_val = random_split(mnist_train, [55000, 5000])
        if stage == "test":
            self.mnist_test = MNIST(os.getcwd(), train=False, transform=transform)
        if stage == "predict":
            self.mnist_predict = MNIST(os.getcwd(), train=False, transform=transform)

    # return the dataloader for each split
    def train_dataloader(self, batch_size=None):
        batch_size = batch_size or self.batch_size
        mnist_train = TransformativeDataLoader(self.mnist_train, batch_size=batch_size, pairing=self.pairing, **self.loader_args)
        return mnist_train

    def val_dataloader(self, batch_size=None):
        batch_size = batch_size or self.batch_size
        mnist_val = TransformativeDataLoader(self.mnist_val, batch_size=batch_size, pairing=self.pairing, **self.loader_args)
        return mnist_val

    def test_dataloader(self, batch_size=None):
        batch_size = batch_size or self.batch_size
        mnist_test = TransformativeDataLoader(self.mnist_test, batch_size=batch_size, pairing=self.pairing, **self.loader_args)
        return mnist_test

    def predict_dataloader(self, batch_size=None):
        batch_size = batch_size or self.batch_size
        mnist_predict = TransformativeDataLoader(self.mnist_predict, batch_size=batch_size, pairing=self.pairing, **self.loader_args)
        return mnist_predict

    # utils callback
    def train_dataset(self):
        return self.mnist_train
    def validation_dataset(self):
        return self.mnist_val
    def test_dataset(self):
        return self.mnist_test
