import torch, torch.nn as nn, torch.distributions as dist, sys, numpy as np, pdb
sys.path.append('../')
from transformative_learning.utils import checklist, print_stats, Config, checkdist
from transformative_learning.modules import layers, modulation


class ConvModulationMorpher(nn.Module):
    MorphLayer = "ConvLayer"
    ModulationLayer = "MLP"

    def __getitem__(self, item):
        if item >= self.nlayers:
            raise IndexError()
        return self.conv_layers[item]

    def __init__(self, config, latent_config):
        super().__init__()
        assert config.input_dim is not None, "input_dim cannot be None"
        self.configure_morpher(config, latent_config)
        self.configure_modulation(config, latent_config)

    def configure_morpher(self, config, latent_config):
        # configuring morpher
        input_dim = config.input_dim
        self.target_dist = None if config.target_dist is None else checkdist(config.target_dist)
        out_channels = self.get_channels_from_dist(input_dim[0], self.target_dist)
        self.channels = [input_dim[0]] + config.channels + [out_channels]
        self.dim = config.dim or len(input_dim) - 1
        self.nlayers = len(self.channels) - 1
        self.kernel_size = checklist(config.kernel_size, n=self.nlayers)
        self.dilation = checklist(config.dilation, n=self.nlayers)
        self.norm = checklist(config.norm, n=self.nlayers)
        self.norm[-1] = "none"
        self.nn_lin = checklist(config.nn_lin, n=self.nlayers)
        self.nn_lin[-1] = None
        self.nn_lin = [None] * len(self.nn_lin)
        self.bias = checklist(config.bias, n=self.nlayers)
        self.dropout = checklist(config.dropout, n=self.nlayers)
        MorphLayer = getattr(layers, config.layer or self.MorphLayer)

        self.conv_layers = nn.ModuleList()
        for n in range(self.nlayers):
            self.conv_layers.append(MorphLayer([self.channels[n], self.channels[n+1]],
                                               dim = self.dim,
                                               kernel_size = self.kernel_size[n],
                                               dilation = self.dilation[n],
                                               padding = int(np.floor(self.kernel_size[n] / 2)),
                                               bias = self.bias[n], dropout=self.dropout[n],
                                               norm = self.norm[n], nn_lin = self.nn_lin[n]))

    def configure_modulation(self, config, latent_config):
        # configuring modulations
        latent_dim = latent_config.dim
        modulation_config = config.modulation or Config(type=self.film_layer, args=Config(nlayers=2, dim=800))
        FilmLayer = getattr(modulation, modulation_config.type or self.ModulationLayer)
        self.film_layers = nn.ModuleList()
        for n in range(self.nlayers):
            self.film_layers.append(FilmLayer(latent_dim, self.channels[n+1], ndim=self.dim, **dict(modulation_config.args)))

    def get_channels_from_dist(self, out_channels, distribution):
        if distribution == dist.Normal:
            return out_channels * 2
        else:
            return out_channels

    def forward(self, x, mod_input=None, trace=None):
        out = x
        for i, module in enumerate(self.conv_layers):
            if mod_input is None:
                out = module(out)
            else:
                trace_mod = {} if trace is not None else None
                mod_closure = self.film_layers[i](mod_input, trace=trace_mod)
                if trace is not None:
                    for k, v in trace_mod.items():
                        trace[f"{k}_{i}"] = v
                out = module(out, mod_closure=mod_closure)
            if trace is not None:
                trace[f'layer_{i}'] = out
        if self.target_dist == dist.Normal:
            mean, std = out.split(out.shape[-(self.dim + 1)] / 2, dim = -self.dim + 1)
            std = torch.sigmoid(std)
            out = dist.Normal(mean, std.sqrt())
            if trace is not None:
                trace['out_mean'] = mean
                trace['out_std'] = std.sqrt()
        elif self.target_dist in [dist.Bernoulli, dist.Categorical]:
            out = (out.tanh() + 1) / 2
            out = dist.Bernoulli(probs=out)
            if trace is not None:
                trace['out_probs'] = out.probs
        return out

class ConvWeightModulationMorpher(ConvModulationMorpher):
    MorphLayer = "DummyConvLayer"
    ModulationLayer = "Conv"

    def configure_modulation(self, config, latent_config):
        latent_dim = latent_config.dim
        modulation_config = config.modulation or Config(type=self.film_layer, args=Config(nlayers=2, dim=800))
        FilmLayer = getattr(modulation, modulation_config.type or self.ModulationLayer)
        self.film_layers = nn.ModuleList()
        for n in range(self.nlayers):
            self.film_layers.append(FilmLayer(latent_dim, module=self.conv_layers[n], **dict(modulation_config.args)))


class BilinearModulationMorpher(nn.Module):
    ModulationLayer = "MLP"

    def __init__(self, 
                 input_shape,
                 latent_dim,
                 dims,
                 nn_lin = "SiLU",
                 dropout = None,
                 bias = False,
                 mod_dims = None,
                 mod_layer = modulation.FiLM,
                 target_dist=None):
        super().__init__()
        self.configure_morpher(input_shape, dims, mod_dims, nn_lin, dropout, bias, target_dist)
        self.configure_modulation(latent_dim, mod_layer)

    def configure_morpher(self, input_shape, dims, mod_dims=None, nn_lin=None, dropout=None, bias=True, target_dist="Normal"):
        self.input_shape = input_shape
        self.dims = dims 
        self.mod_dims = mod_dims or dims
        self.nn_lin = nn_lin
        self.dropout = dropout
        self.bias = bias
        if target_dist is not None:
            self.target_dist = getattr(torch.distributions, target_dist)
        self.target_dist = target_dist
        target_shape = list(input_shape)
        if self.target_dist == dist.Normal:
            target_shape[0] *= 2
        self.target_shape = target_shape
            
        input_shape = np.prod(input_shape).item()
        target_shape = np.prod(target_shape).item()
        dims = [input_shape] + self.dims + [target_shape]
        modules = []
        for i in range(len(dims) - 1):
            nn_lin = None if i == len(dims) - 2 else self.nn_lin
            if i == 0:
                modules.append(layers.MLPLayer(dims[i], dims[i+1], dropout=self.dropout, bias=self.bias, nn_lin=nn_lin))
            else:
                modules.append(layers.BilinearLayer(dims[i], self.mod_dims[i-1], dims[i+1], dropout=self.dropout, bias=self.bias, nn_lin=nn_lin))
        self.layers = nn.ModuleList(modules)

    def configure_modulation(self, latent_dim, mod_layer):
        self.mod_layers = nn.ModuleList()
        for n in range(1, len(self.layers)):
            self.mod_layers.append(mod_layer(latent_dim, self.dims[n-1]))

    def forward(self, x, mod_input=None, trace=None):
        data_shape = x.shape[1:]
        out = x.view(x.size(0), -1)
        for i, module in enumerate(self.layers):
            if i == 0:
                out = module(out)
            else:
                if mod_input is None:
                    out = module(out, out)
                else:
                    trace_mod = {} if trace is not None else None
                    mod = self.mod_layers[i-1](mod_input, trace=trace_mod)
                    if trace is not None:
                        for k, v in trace_mod.items():
                            trace[f"{k}_{i}"] = v
                    try:
                        out = module(out, mod)
                    except RuntimeError:
                        pdb.set_trace()
            if trace is not None:
                trace[f'layer_{i}'] = out
        if self.target_dist == dist.Normal:
            out = out.reshape(-1, *self.target_shape)
            mean, std = out.split(out.shape[-(len(self.target_shape) + 1)] / 2, dim = -(len(self.target_shape) + 1))
            std = torch.sigmoid(std)
            out = dist.Normal(mean, std.sqrt())
            if trace is not None:
                trace['out_mean'] = mean
                trace['out_std'] = std.sqrt()
        elif self.target_dist in [dist.Bernoulli, dist.Categorical]:
            out = (out.sigmoid())
            out = dist.Bernoulli(probs=out.view(out.size(0), *data_shape))
            if trace is not None:
                trace['out_probs'] = out.probs
        else:
            out = out.reshape(-1, *self.target_shape)
            if trace is not None:
                trace['out'] = out
        return out
            
