import torch, torch.nn as nn, pdb, gin.torch
from math import sqrt


@gin.configurable
class MLPModulation(nn.Module):

    def __init__(self, input_dim, output_dim, nlayers=None, dim=None, ndim=1, nn_lin="SiLU", bias=True):
        super().__init__()
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.nlayers = nlayers or 2
        self.nn_lin = nn_lin
        self.hidden_dim = dim or output_dim
        self.bias = bias
        self.ndim = ndim
        dims = [input_dim] + [self.hidden_dim] * (self.nlayers - 1) + [output_dim]
        layers = nn.ModuleList()
        if isinstance(nn_lin, str):
            nn_lin = getattr(nn, nn_lin)
        for i in range(len(dims) - 1):
            layers.append(nn.Linear(dims[i], dims[i+1]))
            if i <len(dims) - 2:
                layers.append(nn_lin())
        self.layers = nn.Sequential(*layers)
        self.init_layers()

    def init_layers(self):
        pass

    def forward(self, mod_input, trace=None):
        return self.layers(mod_input)

class FiLM(MLPModulation):

    def __repr__(self):
        return f"FiLM({self.input_dim}, {self.output_dim}, nlayers={self.nlayers}, nn_lin={self.nn_lin})"

    def forward(self, mod_input, trace=None):
        out = mod_input
        for n in self.layers:
            out = n(out)
        if self.bias:
            weight, bias = out.split(int(out.size(-1) / 2), dim=-1)
            weight = weight.view(*weight.shape, *(1,)*self.ndim)
            bias = bias.view(*bias.shape, *(1,) * self.ndim)
            if trace is not None:
                trace['weight'] = weight
                trace['bias'] = bias
            return lambda x: weight.expand_as(x) * x + bias.expand_as(x)
        else:
            weight = out
            weight = weight.view(*weight.shape, *(1,)*self.ndim)
            if trace is not None:
                trace['weight'] = weight
            return lambda x : weight.expand_as(x) * x


def grouped_conv(weight, module, callback, bias=None):
    #TODO wtf with the first argument
    def _apply_grouped_conv(x):
        out = callback(x.view(1, -1, *x.shape[2:]), weight.view(-1, *module.weight.shape[1:]), stride=module.stride, padding=module.padding, dilation=module.dilation, bias=bias, groups=x.size(0))
        return out.view(x.size(0), -1, *out.shape[2:])
    return _apply_grouped_conv
    
class Conv(MLPModulation):
    conv_hash = {1: nn.functional.conv1d, 2: nn.functional.conv2d, 3: nn.functional.conv3d}
    def __repr__(self) -> str:
        return f"Conv({self.input_dim}, {self.output_dim}, nlayers={self.nlayers}, nn_lin={self.nn_lin})"

    def __init__(self, in_shape, module=None, **kwargs):
        assert module is not None
        conv_module = module.module.conv
        self.weight_shape = conv_module.weight.shape.numel()
        self.bias_shape = 0
        if conv_module.bias:
            self.bias_shape = conv_module.bias.shape.numel()
        super().__init__(in_shape, self.weight_shape + self.bias_shape, bias=False, **kwargs)
        self.__dict__['module'] = module
        if module is None:
            raise NotImplementedError

    def init_layers(self):
        for n in self.layers:
            if isinstance(n, nn.Linear):
                nn.init.xavier_normal_(n.weight)

    def forward(self, mod_input, trace=None):
        out = mod_input
        conv_callback = self.conv_hash[self.module.dim]

        for n in self.layers:
            out = n(out)
        if self.bias_shape != 0:
            raise NotImplementedError
        else:
            weight = out * sqrt(1 / out.size(1))
            if trace is not None:
                trace['weight'] = weight
            return lambda x : grouped_conv(weight, self.module.module.conv, conv_callback)(x)

