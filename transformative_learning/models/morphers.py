import sys; sys.path.append("../")
from typing import Iterable
from transformative_learning.utils import *
from transformative_learning.modules import encoders, morphers
import gin.torch
import torch, torch.nn as nn, pytorch_lightning as pl, torch.distributions as dist


@gin.configurable
class ModulationMorpher(pl.LightningModule):
    available_encoder_inputs = ["concat", "single", "diff"]
    def __init__(self,
                 morpher: nn.Module,
                 encoder: nn.Module,
                 input_shape: Iterable[int],
                 latent_dim: int = 2,
                 latent_dist: str = "Normal", 
                 beta: float = 1.0,
                 warmup : int = 0,
                 encoder_input: str = "concat"):
                 
        super().__init__()
        self.input_shape = input_shape
        self.latent_dim = latent_dim
        self.latent_dist = latent_dist
        self.encoder_input = encoder_input
        self.morpher = morpher
        self.encoder = encoder
        self.beta = beta
        self.warmup = warmup
        
    def forward(self, x, *args, **kwargs):
        return self.encoder(x)

    def get_encoder_input(self, x_1, x_2=None, encoder_input=None):
        encoder_input = encoder_input or self.encoder_input
        if encoder_input == "concat":
            if x_2 is None:
                x_2 = x_1
            return torch.cat([x_1, x_2], dim=-(len(self.input_shape)))
        elif encoder_input == "single":
            return x_2
        elif encoder_input == "diff":
            if x_2 is None:
                x_2 = x_1
            return torch.abs(x_1 - x_2)

    @gin.configurable
    def configure_optimizers(self, lr:float=1.e-4):
        optimizer = torch.optim.Adam(self.parameters(), lr)
        return optimizer

    def sample_latent(self, z):
        if isinstance(z, dist.Distribution):
            if z.has_rsample:
                z = z.rsample()
            else:
                z = z.sample()
        return z

    def morph(self, x_1, x_2, trace=None):
        if x_1.size(0) == 1:
            x_1 = x_1.expand_as(x_2)
        x_encoder = self.get_encoder_input(x_1, x_2, self.encoder_input)
        z = self.encoder(x_encoder)
        out = self.morpher(x_1, mod_input=self.sample_latent(z), trace=trace)
        return out, z

    def encode(self, x_1, x_2=None):
        encoder_input = self.get_encoder_input(x_1, x_2, self.encoder_input)
        return self.encoder(encoder_input)

    def sample_morphs(self, x_1, n_samples, z=None):
        batch_size= x_1.size(0)
        if z is None:
            z = torch.randn((1, n_samples, self.latent_dim), device=x_1.device).repeat(x_1.size(0), 1, 1)
        x_1 = x_1.unsqueeze(1).repeat(1, n_samples, *(1,)*len(x_1.shape[1:]))
        x_1 = x_1.reshape(x_1.size(0) * n_samples, *x_1.shape[2:])
        z = z.reshape(z.size(0) * n_samples, *z.shape[2:])
        out = self.morpher(x_1, mod_input=z)
        if isinstance(out, dist.Bernoulli):
            out = dist.Bernoulli(probs=out.probs.reshape(batch_size, n_samples, *x_1.shape[1:]))
        elif isinstance(out, dist.Normal):
            out = dist.Normal(out.mean.reshape(batch_size, n_samples, *x_1.shape[1:]),
                              out.stddev.reshape(batch_size, n_samples, *x_1.shape[1:]))
        else:
            out = out.reshape(batch_size, n_samples, *x_1.shape[1:])
        return out, z

    def process(self, batch, batch_idx):
        (x_1, x_2), (y_1, y_2) = batch
        out, z = self.morph(x_1, x_2)
        # reconstruction loss
        if isinstance(out, dist.Distribution):
            loss_rec = -out.log_prob(x_2)
        else:
            loss_rec = torch.nn.functional.mse_loss(out, x_2, reduction="none")
        loss_rec = loss_rec.mean(0).sum()
        # regularization loss
        loss_reg = dist.kl.kl_divergence(z, dist.Normal(torch.zeros_like(z.mean), torch.ones_like(z.stddev)))
        loss_reg = loss_reg.mean(0).sum()
        # full loss
        beta = self.beta
        if self.warmup > 0:
            beta = max(beta, self.current_epoch / self.warmup * beta)
        full_loss = loss_rec + beta * loss_reg
        return full_loss, (loss_rec, loss_reg)

    def training_step(self, batch, batch_idx):
        full_loss, (loss_rec, loss_reg) = self.process(batch, batch_idx)
        self.log("rec_loss/train", loss_rec, on_step=False, on_epoch=True, prog_bar=False)
        self.log("reg_loss/train", loss_reg, on_step=False, on_epoch=True, prog_bar=False)
        self.log("loss", full_loss, on_step=False, on_epoch=True, prog_bar=True)
        return full_loss

    def validation_step(self, batch, batch_idx):
        full_loss, (loss_rec, loss_reg) = self.process(batch, batch_idx)
        self.log("rec_loss/valid", loss_rec, on_step=False, on_epoch=True, prog_bar=False)
        self.log("reg_loss/valid", loss_reg, on_step=False, on_epoch=True, prog_bar=False)
        self.log("loss/valid", full_loss, on_step=False, on_epoch=True, prog_bar=True)
        return full_loss









