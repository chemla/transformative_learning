import numpy as np, torch, torch.nn as nn, torch.nn.functional as F, torch.distributions as dist
import pytorch_lightning as pl


def get_inputs(x_1, x_2, prob=0.2):
    mask = dist.Bernoulli(torch.ones(x_1.size(0)) * prob).sample().bool()
    mask = mask.__getitem__((slice(None), *(np.newaxis,)*(len(x_1.shape) - 1))).expand_as(x_1)
    return torch.where(mask, x_1, x_2)


class AutoEncoder(pl.LightningModule):
    def __init__(self):
        super().__init__()
        self.encoder = nn.Sequential(nn.Linear(28 * 28, 64), nn.ReLU(), nn.Linear(64, 3))
        self.decoder = nn.Sequential(nn.Linear(3, 64), nn.ReLU(), nn.Linear(64, 28 * 28))

    def forward(self, x):
        # in lightning, forward defines the prediction/inference actions
        embedding = self.encoder(x)
        return embedding

    def reconstruct(self, x, batch, batch_idx):
        x_input = x.view(x.size(0), -1)
        z = self.encoder(x_input)
        x_hat = self.decoder(z).view(x.shape)
        return x_hat

    def training_step(self, batch, batch_idx):
        # training_step defined the train loop.
        # It is independent of forward
        (x_1, x_2), (y_1, y_2) = batch
        x_hat = self.reconstruct(x_1, batch, batch_idx)
        x_target = get_inputs(x_1, x_2, 0.2)
        loss = F.mse_loss(x_hat, x_target)
        # Logging to TensorBoard by default
        self.log("loss", {'train':loss}, on_step=False, on_epoch=False, prog_bar=True)
        return loss

    def validation_step(self, batch, batch_idx):
        (x_1, x_2), (y_1, y_2) = batch
        x_hat = self.reconstruct(x_1, batch, batch_idx)
        x_target = get_inputs(x_1, x_2, 0.2)
        loss = F.mse_loss(x_hat, x_target)
        self.log("loss", {'valid':loss}, on_step=False, on_epoch=False, prog_bar=True)
        return loss

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=1e-3)
        return optimizer